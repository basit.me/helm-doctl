Use this repository if you need to deploy with helm and your cluster is on digitalocean.

Example:
```
docker run -e DIGITALOCEAN_ACCESS_TOKEN=<token> aronwolf/helm-doctl \
  sh -c "doctl kubernetes cluster kubeconfig save <clustername>; helm install wordpress"
```

#  Links
Repository: https://gitlab.com/web_utils/helm-doctl
